# Sets global variables for this Terraform project.

variable "app_name" {
  default = "flixtube6013"
}
variable "location" {
  default = "Southeast Asia"
}

variable "admin_username" {
  default = "linux_admin"
}

variable "app_version" {
}

variable "client_id" {
  
}

variable "client_secret" {
}
